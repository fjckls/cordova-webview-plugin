package webview;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.SupportActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import lv.mobilly.mobile.R;

public class WebViewActivity extends SupportActivity {

    private WebView mWebView;
    private boolean loaded=false;
    private String urlContext;
    private static final String webViewClosed="webViewClosed";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wview);
        String _url = getIntent().getStringExtra(webview.WebView.DATA_WEBPAGE_URL);
        urlContext = getIntent().getStringExtra(webview.WebView.DATA_CONTEXT_URL);
        String _title = getIntent().getStringExtra(webview.WebView.DATA_TITLE);
        ((TextView) findViewById(R.id.title_text)).setText(_title);
        findViewById(R.id.close_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webview.WebView.sendResult(webViewClosed);
                finish();
            }
        });
        showWebView(_url);
    }


    private  void showWebView(String _url) {
        if(!loaded){
            mWebView = (WebView) findViewById(R.id.wview);
            mWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String _redirectTo) {
                    String _searchFor = String.format("%s:", urlContext);
                    if(_redirectTo.toLowerCase().contains(_searchFor.toLowerCase())){
                        _redirectTo = _redirectTo.replace(String.format("%s://", urlContext), "").replace(String.format("%s:", urlContext), "");
                        webview.WebView.sendResult(_redirectTo);
                        finish();
                        return true; // Don't allow redirect
                    }
                    return false; // Allow redirect
                }
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    findViewById(R.id.progress_round).setVisibility(View.GONE);
                }
            });
            mWebView.setVisibility(View.VISIBLE);
            mWebView.clearCache(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                WebView.setWebContentsDebuggingEnabled(true);
            WebSettings webSettings = mWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            mWebView.loadUrl(_url);
        }
    }
}

