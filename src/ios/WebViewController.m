#import "WebViewController.h"

@interface WebViewController()

@property (strong, nonatomic, nonnull) WKWebView *webView;

@property (strong, nonatomic, nonnull) UIActivityIndicatorView *activityIndicatorView;

@end

@implementation WebViewController

- (void)loadView
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    view.backgroundColor = [UIColor colorWithWhite:0.98 alpha:1];

    WKWebViewConfiguration *configuration = [WKWebViewConfiguration new];
    configuration.suppressesIncrementalRendering = YES;

    _webView = [[WKWebView alloc] initWithFrame:view.bounds configuration:configuration];
    [self setupWebViewInContainer:view];

    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self setupActivityIndicatorInContainer:view];

    self.view = view;
}

- (void)setupWebViewInContainer:(UIView *)view {
    self.webView.navigationDelegate = self;
    self.webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.webView.alpha = 0.0;
    [view addSubview:self.webView];
}

- (void)setupActivityIndicatorInContainer:(UIView *)view {
    self.activityIndicatorView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.activityIndicatorView.center = self.webView.center;
    [self.activityIndicatorView startAnimating];
    self.activityIndicatorView.color = [UIColor blackColor];
    [view addSubview:self.activityIndicatorView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close-button"] style:UIBarButtonItemStylePlain target:self action:@selector(close)];

    [self.webView loadRequest:[NSURLRequest requestWithURL:self.url]];
}

- (void)close
{
    [self.delegate webViewControllerDidFinish:self];
}

#pragma MARK - WKNavigationDelegate

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    [self.activityIndicatorView stopAnimating];
    [UIView animateWithDuration:0.25 animations:^{
        self.webView.alpha = 1.0;
    } completion:^(BOOL finished) {
        if (finished) [self.webView.scrollView flashScrollIndicators];
    }];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    if ([self parseRequest:navigationAction.request]) {
        decisionHandler(WKNavigationActionPolicyCancel);
    } else {
        decisionHandler(WKNavigationActionPolicyAllow);
    }
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    [self handleError];
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    [self handleError];
}

#pragma MARK -

- (void)handleError
{
    [self.activityIndicatorView stopAnimating];
    [self.delegate webViewControllerDidFailToLoadContent:self];
}

#pragma MARK -

- (BOOL)parseRequest:(NSURLRequest *)request {
    if ([request.URL.scheme isEqualToString:self.callbackScheme]) {
        [self.delegate webViewControllerDidCaptureCommand:self command:request.URL.query];
        return YES;
    }
    return NO;
}

@end
