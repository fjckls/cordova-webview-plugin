#import "Webview.h"
#import "WebViewController.h"

@interface UIImage (Extras)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end

@implementation UIImage (Extras)

+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, UIScreen.mainScreen.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, rect);

    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}

@end

@implementation Webview
@synthesize callbackID;

- (void)present:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        self.callbackID = command.callbackId;

        WebViewController *webViewController = [WebViewController new];
        webViewController.url = [NSURL URLWithString:[command.arguments objectAtIndex:0]];
        webViewController.callbackScheme = [command.arguments objectAtIndex:1];
        webViewController.delegate = self;
        webViewController.title = [command.arguments objectAtIndex:2];

        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:webViewController];
        [self configureNavigationBarFor:navController];

        dispatch_async(dispatch_get_main_queue(), ^{
            [self.viewController presentViewController:navController animated:YES completion:nil];
        });
    }];
}

- (void)configureNavigationBarFor:(UINavigationController *)navigationController {
    UINavigationBar *navigationBar = navigationController.navigationBar;

    // Sets color of close button
    navigationBar.tintColor = [UIColor blackColor];

    navigationBar.translucent = NO;
    [navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    // For some reason this is not working yet
//    navigationBar.shadowImage = [UIImage imageWithColor:[UIColor colorWithWhite:241/255 alpha:1]];
}

- (void)webViewControllerDidCaptureCommand:(WebViewController *)webViewController command:(NSString *)command
{
    [self closeWebViewController:webViewController withEvent:command isError:NO];
}

- (void)webViewControllerDidFinish:(WebViewController *)webViewController
{
    [self closeWebViewController:webViewController withEvent:@"webViewClosed" isError:NO];
}

- (void)webViewControllerDidFailToLoadContent:(WebViewController *)webViewController
{
    [self closeWebViewController:webViewController withEvent:@"webViewFailedToLoadContent" isError:YES];
}

- (void)closeWebViewController:(WebViewController *)webViewController withEvent:(NSString *)event isError:(BOOL)isError
{
    [self.viewController dismissViewControllerAnimated:YES completion:^{
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:isError ? CDVCommandStatus_ERROR : CDVCommandStatus_OK
                                                          messageAsString:event];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:self.callbackID];
    }];
}

@end
