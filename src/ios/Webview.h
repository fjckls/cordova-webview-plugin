#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>
#import "WebViewController.h"

@interface Webview : CDVPlugin<WebViewControllerDelegate>

@property(strong) NSString* callbackID;

- (void)present:(CDVInvokedUrlCommand*)command;

@end
