'use strict';

var exec = require('cordova/exec');

var webview = {};

webview.present = function(url, scheme, title, success, failure) {
    // fire
    exec(
        success,
        failure,
        'Webview',
        'present', [url, scheme, title]
    );
};

module.exports = webview;
