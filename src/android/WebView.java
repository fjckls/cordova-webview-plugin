package webview;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;

public class WebView extends CordovaPlugin {

    public static final String DATA_WEBPAGE_URL = "DATA_WEBPAGE_URL";
    public static final String DATA_CONTEXT_URL = "DATA_CONTEXT_URL";
    public static final String DATA_TITLE = "DATA_TITLE";
    private static WebView currentInstance = null;
    private CallbackContext callbackContext;

    @Override
    public void initialize (CordovaInterface cordova, CordovaWebView webView) {
        WebView.currentInstance = this;
    }

    @Override
    public boolean execute(String _action, JSONArray _args, CallbackContext _callbackContext) throws JSONException {
        if (_action.equals("present")) {
            String _webpage = _args.getString(0);
            String _context = _args.getString(1);
            String _title = _args.getString(2);
            callbackContext = _callbackContext;
            Context context=this.cordova.getActivity().getApplicationContext();
            Intent intent=new Intent(context,WebViewActivity.class);
            intent.putExtra(DATA_WEBPAGE_URL, _webpage);
            intent.putExtra(DATA_CONTEXT_URL, _context);
            intent.putExtra(DATA_TITLE, _title);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            cordova.startActivityForResult(this,intent,0);
            return true;
        }else if(_action.equals("close")){
            return true;
        }
        return false;
    }

    public static void sendResult(String _result) {
        if(WebView.currentInstance!=null && WebView.currentInstance.callbackContext !=null)
            WebView.currentInstance.callbackContext.success(_result);
    }
}
